Title: GenBank
Author: Shola Esho

# GenBank

1. [Overview](#overview)
2. [Setup](#setup)
3. [The Apporach](#the-approach)


## Overview

This program outputs occurrences of a pattern from a sequence given an NCBI database and a sequence UID.


## Setup

1. Install the requirements:

    $ pip install -r requirements.pip

2. Run the application:

    a. Enter the command replacing the items within the angled brackets with their respected values:

        $ ./simons/gencode.py <DATABASE> <UID> <OUTPUT> <PATTERN>

    b. Optionally, you can supply a max records per file value resulting in multiple output files. The default is 500000:

        $ ./simons/gencode.py -m <MAX RECORDS PER FILE> <DATABASE> <UID> <OUTPUT> <PATTERN>

    c. For additional usage instructions:

        $ ./simons/gencode.py -h

3. Run the tests:

    $ python simons/tests.py


## The Approach

After reading through the instructions a few times to properly gather and understand the requirements, I decided on what libraries I might need to derive a solution. For the XML library, I'd previously worked with BeautifulSoup, lxml2 and the Etree library. Given the simplicity of the job (retrieving the contents of a tag within an XML document), I settled on the no-frills Python native Etree though it may not be the most performant of the bunch. For the HTTP requests to the NCBI database, I looked no further than the requests library. It removes the insanity out of making json-based API requests and I found that it works well with the NCBI XML API.

Following the tools selection, I began outlining the program composition. I wanted the components to be minimal in scope, complete and composable resulting in an easily testable environment where components are replaceable. With that mindset in place, I wrote the function signatures, implemented the test cases followed by the program implementation.

The biggest design decision was how to output the pattern matching data (both to stdout and the destination file). I initially thought to create an array in memory to hold matched sequences and then later output the data. To reduce the program's runtime and, most importantly, memory footprint, I eventually decided to utilize a generator in order to output the data as the matches are found.

While testing out program on a few test cases, I noticed that the output file can get quite large depending on the input. With that in mind, I refactored output components to allow the user to supply an optional value to specify the maximum records per file. When that limit is reached, the output is continued onto a new file of the format OUTPUT_FILENAME.NUMBER where NUMBER indicates an incrementing number starting with 1.

Other considerations not implemented include allowing the user to supply a start and stop position to support restartability of the program and a means to disable the stdout stream for unattended executions. In addition, the output file could be compressed to allow for portability. For performance, considerations not implemented included caching the parsed sequence data locally to short-circuit some of the processing when dealing with reoccurring sequence UIDs. These could be followed through based on factors such as resource availability, more detailed use cases and external dependencies (eg external API restrictions).
