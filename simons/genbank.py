#!/usr/bin/env python
"""
Outputs occurrences of a pattern from a sequence given an NCBI database
and a sequence UID.

"""

import argparse
import re
import sys

from xml.etree import ElementTree

import requests


DEFAULT_MAX_RECORDS = 500000
NCBI_EFETCH_URL = (
    'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi'
)
NCBI_EFETCH_PARAMS = {
    'rettype': 'fasta',
    'retmode': 'xml',
}


class GenBankError(Exception):
    """Base error for genbank module"""
    pass


class GenBankParseError(GenBankError):
    """Parsing error for genbank module"""
    pass


class GenBankConnectError(GenBankError):
    """External connection error for genbank module"""
    pass


def fetch_sequence(database, uid):
    """
    Fetches the sequence data for a give database and sequence UID from
    the NCBI Database.

    Args:
        database (str): source database name
        uid (int): source genome UID

    Returns:
        (str): XML formatted sequence data
    """
    params = dict(db=database, id=uid, **NCBI_EFETCH_PARAMS)
    response = requests.get(NCBI_EFETCH_URL, params=params)

    if not response.ok:
        raise GenBankConnectError(
            "Error requesting data on url: %s" % response.url)

    return response.content


def parse_sequence(content):
    """
    Parser for XML formatted sequence data. Raises
    GenBankParseError if parsing errors are encountered.

    Args:
        content (str): XML string containing sequence

    Returns:
        (str): sequence string
    """
    root = ElementTree.fromstring(content)
    if root.tag != 'TSeqSet':
        raise GenBankParseError(
            "Expected tag 'TseqSet', received '%s'" % root.tag)

    for child in root:
        if child.tag != 'TSeq':
            raise GenBankParseError(
                "Expected tag 'Tseq', received '%s'" % child.tag)

        for item in child:
            if item.tag == 'TSeq_sequence':
                return item.text

        raise GenBankParseError(
            "Cannot locate 'TSeq_sequence' tag")

    raise GenBankError(
        "Error parsing data for sequence")


def find_pattern_matches(sequence, pattern, offset=0):
    """
    Finds matches of pattern within sequence

    Args:
        sequence (str): a genome sequence
        pattern (str): a regular expression string

    Keyward args:
        offset (int): index in sequence to start matching. defaults to 0.

    Returns:
        (generator): a generator returning 2-element tuples (x, y) with
        inclusive start (x) and end (y) positions of pattern matches
    """
    if not sequence or not pattern:
        raise StopIteration

    if offset:
        sequence = sequence[offset:]

    for match in re.finditer(pattern, sequence):
        start = match.start() + offset + 1
        end = match.end() + offset

        yield (start, end)


def generate_output(sequence, pattern, output, max_records=DEFAULT_MAX_RECORDS):
    """
    Outputs the pattern matches in output file and to stdout

    Args:
        sequence (str): a genome sequence
        pattern (str): a regular expression string
        output (str): the file path to write match results

    Keyward args:
        max_records: the max records per file. defaults to
            the genbank.DEFAULT_MAX_RECORDS value

    Returns:
        (int): the number matches found
    """
    match_count = 0
    file_part = 0
    match_end = 0

    while True:
        dest_file = '%s.%d' % (output, file_part) if file_part else output
        file_part += 1
        match_iter = find_pattern_matches(sequence, pattern, offset=match_end)

        with open(dest_file, 'w') as fsout:
            for count, match in enumerate(match_iter, 1):
                match_end = match[1]

                print '%s\t%s' % match
                fsout.write('%s,%s\n' % match)
                match_count += 1

                if max_records and count >= max_records:
                    break
            else:
                # break out of while-loop if limit isn't exceeded
                break


    print "\nMatching sequences found: %d" % match_count

    return match_count


def _main(database, uid, output, pattern, max_records):
    """Main sequence"""
    content = fetch_sequence(database, uid)
    sequence = parse_sequence(content)
    generate_output(sequence, pattern, output, max_records=max_records)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument(
        'database', type=str, help="source GenBank database name")
    parser.add_argument('id', type=int, help="source genome UID")
    parser.add_argument('output', type=str, help="csv destination file")
    parser.add_argument('pattern', type=str, help="matching regular expression")
    parser.add_argument(
        '-m', '--maxrecs', type=int, default=DEFAULT_MAX_RECORDS,
        help=("maximum records per file. Default is %s. Set to 0 for no limit. "
              "Extension files will have format output.x where x is 1 or "
              "greater") % DEFAULT_MAX_RECORDS)

    args = parser.parse_args()

    try:
        _main(args.database, args.id, args.output, args.pattern, args.maxrecs)
    except GenBankError as error:
        sys.stderr.write("GENBANK ERROR - %s\n" % str(error))
        sys.exit(1)
