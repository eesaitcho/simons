import os
import sys
import unittest

from cStringIO import StringIO
from uuid import uuid4
from xml.etree.ElementTree import ParseError

from simons import genbank


MOCK_SEQUENCE = 'GGAGAATTAGACAAAAAAATTAAAAAAAATTAAAAAAAA'
MOCK_PATTERN = 'ATTA'
MOCK_RESULTS = (
    (6, 9),
    (19, 22),
    (29, 32),
)
MOCK_RESULTS_COUNT = 3

SAMPLE_DB = 'nucleotide'
SAMPLE_UID = 30271926
SAMPLE_PATTERN = 'ATTA'
TMP_DIR = '/tmp'


class TestFetchSequence(unittest.TestCase):
    """Tests genbank.fetch_sequence call"""
    def test_base_case(self):
        """
        Tests fetch_sequence base case with request using known database
        and uid value
        """
        db = SAMPLE_DB
        uid = SAMPLE_UID

        results = genbank.fetch_sequence(db, uid)
        self.assertIsNotNone(results)
        self.assertTrue(len(results) > 0)

    def test_invalid_case(self):
        """
        Tests fetch_sequence invalid case with request using known
        invalid database and uid value expecting a raised exception
        """
        db = ''
        uid = 0
        with self.assertRaises(genbank.GenBankConnectError):
            genbank.fetch_sequence(db, uid)


class TestParseSequence(unittest.TestCase):
    """Tests genbank.parse_sequence call"""
    def test_base_case(self):
        """Test parse_sequence base case with valid content"""
        sequence = 'ATATTAGGTTTTTACCTACCAAAGCCAACCAACC'
        content = (
            '<?xml version="1.0"?>'
            '<!DOCTYPE TSeqSet PUBLIC "-//NCBI//NCBI TSeq/EN" '
            '"http://www.ncbi.nlm.nih.gov/dtd/NCBI_TSeq.dtd">'
            '<TSeqSet>'
            '<TSeq>'
            '<TSeq_seqtype value="nucleotide"/>'
            '<TSeq_gi>30271926</TSeq_gi>'
            '<TSeq_accver>NC_004718.3</TSeq_accver>'
            '<TSeq_sid>gnl|NCBI_GENOMES|17014</TSeq_sid>'
            '<TSeq_taxid>227859</TSeq_taxid>'
            '<TSeq_orgname>SARS coronavirus</TSeq_orgname>'
            '<TSeq_defline>SARS coronavirus, complete genome</TSeq_defline>'
            '<TSeq_length>29751</TSeq_length>'
            '<TSeq_sequence>%s</TSeq_sequence>'
            '</TSeq>'
            '</TSeqSet>'
        ) % sequence

        results = genbank.parse_sequence(content)
        self.assertEqual(sequence, results)

    def test_no_tseqset(self):
        """Test parse_sequence invalid case with no TSeqSet"""
        content = (
            '<?xml version="1.0"?>'
            '<!DOCTYPE TSeqSet PUBLIC "-//NCBI//NCBI TSeq/EN" '
            '"http://www.ncbi.nlm.nih.gov/dtd/NCBI_TSeq.dtd">'
            '<BlahSet>'
            '<TSeq>'
            '<TSeq_seqtype value="nucleotide"/>'
            '<TSeq_gi>30271926</TSeq_gi>'
            '<TSeq_accver>NC_004718.3</TSeq_accver>'
            '<TSeq_sid>gnl|NCBI_GENOMES|17014</TSeq_sid>'
            '<TSeq_taxid>227859</TSeq_taxid>'
            '<TSeq_orgname>SARS coronavirus</TSeq_orgname>'
            '<TSeq_defline>SARS coronavirus, complete genome</TSeq_defline>'
            '<TSeq_length>29751</TSeq_length>'
            '<TSeq_sequence>ATTA</TSeq_sequence>'
            '</TSeq>'
            '</BlahSet>'
        )
        with self.assertRaises(genbank.GenBankParseError):
            genbank.parse_sequence(content)

    def test_empty_tseqset_children(self):
        """Test parse_sequence invalid case with no TSeqSet children"""
        content = (
            '<?xml version="1.0"?>'
            '<!DOCTYPE TSeqSet PUBLIC "-//NCBI//NCBI TSeq/EN" '
            '"http://www.ncbi.nlm.nih.gov/dtd/NCBI_TSeq.dtd">'
            '<TSeqSet>'
            '</TSeqSet>'
        )
        with self.assertRaises(genbank.GenBankError):
            genbank.parse_sequence(content)

    def test_no_tseq(self):
        """
        Test parse_sequence invalid case of a TseqSet with no TSeq
        children
        """
        content = (
            '<?xml version="1.0"?>'
            '<!DOCTYPE TSeqSet PUBLIC "-//NCBI//NCBI TSeq/EN" '
            '"http://www.ncbi.nlm.nih.gov/dtd/NCBI_TSeq.dtd">'
            '<TSeqSet>'
            '<Blah>'
            '</Blah>'
            '</TSeqSet>'
        )
        with self.assertRaises(genbank.GenBankParseError):
            genbank.parse_sequence(content)

    def test_no_tseq_sequence(self):
        """
        Test parse_sequence invalid case of Tseq without TSeqSequence
        """
        content = (
            '<?xml version="1.0"?>'
            '<!DOCTYPE TSeqSet PUBLIC "-//NCBI//NCBI TSeq/EN" '
            '"http://www.ncbi.nlm.nih.gov/dtd/NCBI_TSeq.dtd">'
            '<BlahSet>'
            '<TSeq>'
            '<TSeq_seqtype value="nucleotide"/>'
            '<TSeq_gi>30271926</TSeq_gi>'
            '<TSeq_accver>NC_004718.3</TSeq_accver>'
            '<TSeq_sid>gnl|NCBI_GENOMES|17014</TSeq_sid>'
            '<TSeq_taxid>227859</TSeq_taxid>'
            '<TSeq_orgname>SARS coronavirus</TSeq_orgname>'
            '<TSeq_defline>SARS coronavirus, complete genome</TSeq_defline>'
            '<TSeq_length>29751</TSeq_length>'
            '</TSeq>'
            '</BlahSet>'
        )
        with self.assertRaises(genbank.GenBankError):
            genbank.parse_sequence(content)

    def test_invalid_xml(self):
        """Test parse_sequence invalid case with invalid XML content"""
        content = "{}"
        with self.assertRaises(ParseError):
            genbank.parse_sequence(content)

    def test_empty_content(self):
        """Test parse_sequence invalid case with empty content"""
        content = ""
        with self.assertRaises(ParseError):
            genbank.parse_sequence(content)


class TestPatternMatches(unittest.TestCase):
    """Tests genbank.fetch_sequence call"""
    def test_base_case(self):
        """Test find_pattern_matches base case with valid content"""
        sequence = 'GGAGAATGACAAAAAAAAAAAAAAAAAAAAAAAA'
        pattern = "GA"

        expected = [(2, 3), (4, 5), (8, 9)]
        results = list(genbank.find_pattern_matches(sequence, pattern))

        self.assertEqual(expected, results)

    def test_no_matches(self):
        """Test find_pattern_matches with no matches"""
        sequence = 'GGAGAATGACAAAAAAAAAAAAAAAAAAAAAAAA'
        pattern = "CT"

        expected = []
        results = list(genbank.find_pattern_matches(sequence, pattern))

        self.assertEqual(expected, results)

    def test_empty_sequences(self):
        """Test find_pattern_matches invalid case with empty sequence"""
        sequence = 'GGAGAATGACAAAAAAAAAAAAAAAAAAAAAAAA'
        pattern = ""

        expected = []
        results = list(genbank.find_pattern_matches(sequence, pattern))

        self.assertEqual(expected, results)

    def test_empty_pattern(self):
        """Test find_pattern_matches invalid case with empty pattern"""
        sequence = ""
        pattern = "CAT"

        expected = []
        results = list(genbank.find_pattern_matches(sequence, pattern))

        self.assertEqual(expected, results)


class BaseOutputTestClass(unittest.TestCase):
    """Base case for tests that generate files"""
    def setUp(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        self._output = '%s/simons_genbank.%s.csv' % (TMP_DIR, (uuid4().hex))
        self._outputs = {self._output}

    def tearDown(self):
        sys.stdout = self._stdout

        for output in self._outputs:
            if os.path.exists(output):
                os.remove(output)


class TestGenerateOutput(BaseOutputTestClass):
    """Tests genbank.generate_output call"""
    def test_base_call_check_count(self):
        """
        Test run through genbank.generate_output checking resulting counts
        """
        output = self._output

        expected = MOCK_RESULTS_COUNT
        results = genbank.generate_output(MOCK_SEQUENCE, MOCK_PATTERN, output)

        self.assertEqual(expected, results)

    def test_base_call_check_data(self):
        """
        Test run through genbank.generate_output comparing file and
        stdout data with expected
        """
        output = self._output

        expected = list(MOCK_RESULTS)
        genbank.generate_output(MOCK_SEQUENCE, MOCK_PATTERN, output)

        stdout_data = [
            tuple([int(pos) for pos in line.split('\t')])
            for line in self._stringio.getvalue().strip().split('\n')[:-2]
        ]
        with open(output) as fsin:
            fsout_data = [
                tuple([int(pos) for pos in line.split(',')])
                for line in fsin.readlines()
                if line.strip()
            ]

        self.assertEqual(expected, stdout_data)
        self.assertEqual(expected, fsout_data)

    def test_base_call_file_split(self):
        """
        Test run through genbank.generate_output comparing data on
        on a two-file split, stdout and expected
        """
        output = self._output
        output_1 = '%s.1' % output
        self._outputs.add(output_1)

        expected = list(MOCK_RESULTS)
        max_records = 2
        genbank.generate_output(MOCK_SEQUENCE, MOCK_PATTERN, output,
                                max_records=max_records)

        stdout_data = [
            tuple([int(pos) for pos in line.split('\t')])
            for line in self._stringio.getvalue().strip().split('\n')[:-2]
        ]

        with open(output) as fsin:
            fsout_data = [
                tuple([int(pos) for pos in line.split(',')])
                for line in fsin.readlines()
                if line.strip()
            ]
        self.assertTrue(max_records >= len(fsout_data) > 0)

        with open(output_1) as fsin:
            fsout_data_1 = [
                tuple([int(pos) for pos in line.split(',')])
                for line in fsin.readlines()
                if line.strip()
            ]

        self.assertTrue(max_records >= len(fsout_data_1) > 0)

        total_fsout_data = fsout_data + fsout_data_1

        self.assertEqual(expected, stdout_data)
        self.assertEqual(expected, total_fsout_data)


class TestMainSequence(BaseOutputTestClass):
    """Tests genbank main sequence"""
    def test_main_sequence(self):
        """
        Test run through genbank main sequence ensuring output equals
        stdout
        """
        output = self._output
        genbank._main(SAMPLE_DB, SAMPLE_UID, output, SAMPLE_PATTERN,
                      genbank.DEFAULT_MAX_RECORDS)

        stdout_count = len(self._stringio.getvalue().strip().split('\n')) - 2
        with open(output) as fsin:
            lines = [line for line in fsin.readlines() if line.strip()]
            output_count = len(lines)

        self.assertEqual(stdout_count, output_count)


if __name__ == '__main__':
    unittest.main()
